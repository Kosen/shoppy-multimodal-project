package application.model;

import java.util.ArrayList;


public class SpeechRecognizer {
	
		private ArrayList<String> quitKeywords = new ArrayList<String>();
		private String grammarPath;
		private String grammarName;
		
		public SpeechRecognizer(String path, String name) {
			this.grammarPath = path;
			this.grammarName = name;

		}
		
		public boolean containsQuitKeyword (String result) {
			for (String key: quitKeywords) {
				if ( result.indexOf(key)!=-1)
					return true;
			}
			return false;
		}
		
		public void setQuitKeywords (ArrayList<String> keys) {
			this.quitKeywords = keys;
		}
		
		public String recognizeSpeech() {

			voce.SpeechInterface.init("./external_lib/voce/lib", false, true, grammarPath, grammarName);

			
		
			System.out.println("Shoppy is listening! " );

			String speech ="";
			boolean quit = false;
			while (!quit)
			{
				
				try
				{
					Thread.sleep(200);
				}
				catch (InterruptedException e)
				{
				}

				while (voce.SpeechInterface.getRecognizerQueueSize() > 0)
				{
					speech+= voce.SpeechInterface.popRecognizedString();
					

					if (containsQuitKeyword(speech))
					{
						quit = true;
					}

					System.out.println("You said: " + speech);
				}
			}

			voce.SpeechInterface.destroy();
			return speech;
		}
		
		public void deactivate() {
			System.out.println("Deactivating recognizer");
			if (voce.SpeechInterface.isRecognizerEnabled())
				voce.SpeechInterface.destroy();
	
		}
}




