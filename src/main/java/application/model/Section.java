package application.model;

public class Section{
	private String name;
	private String id;
	
	public Section(String name, String id) 
	{
		this.name = name;
		this.id = id;  // in order to localise the section on the map
		
	}

	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return "Section [name=" + name + ", id=" + id + "]";
	}
	
	@Override
	public boolean equals(Object other) {
		return ((Section)other).getName().equals(this.getName());
	}
}