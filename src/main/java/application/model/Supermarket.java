package application.model;

import java.util.ArrayList;
import java.util.HashMap;

import application.view.MapSupermarket;


public class Supermarket {
	
	private  final HashMap<String,Section> products = new HashMap<String,Section>();
	private  final HashMap<String,Section> sections = new HashMap<String,Section>(); 
	private final Grammar supermarketGrammar;
	private final ArrayList<Activity> activities;
	private final SpeechRecognizer recognizer;
	public Supermarket() 
	{
				Section fish = new Section("fish", "sec_fish");
				sections.put("fish", fish );
				Section pharmacy = new Section("pharmacy", "sec_pharmacy");
				sections.put("pharmacy", pharmacy);
				Section vegetables = new Section("vegetables", "sec_vegetables");
				sections.put("vegetables", vegetables );
				
				// Products
						
				products.put( "tuna", fish  );
				products.put( "condoms", pharmacy ); 
				products.put( "tomato", vegetables ); 
		
				supermarketGrammar = new Grammar();
				supermarketGrammar.addWord("guide me to", "navigation");
				supermarketGrammar.addWord("take me to", "navigation");
				supermarketGrammar.addWord("find", "search");
				supermarketGrammar.addWord("where", "search");
				supermarketGrammar.addWord("search", "search");
				supermarketGrammar.addWord("select", "select");
				supermarketGrammar.addWord("please", "quit");
				supermarketGrammar.addWord("thanks", "quit");

				activities = new ArrayList<Activity>();
				activities.add(new Activity("search","product","SearchProduct"));
				activities.add(new Activity("select","section","SelectSection"));
				activities.add(new Activity("navigation","product","Navigating"));
				activities.add(new Activity("navigation","section","Navigating"));
				
				recognizer = new SpeechRecognizer("./external_lib/voce/grammar", "vocab"); //I couldn't move this file without causing problems 
				
				ArrayList<String> quitKeywords = supermarketGrammar.getWordsByCategory("quit");
				recognizer.setQuitKeywords(quitKeywords);
				

	}

	private String getCategory(String word) {
		return supermarketGrammar.getCategory(word);
	}
	
	
	public  Section getSection(String sectionName) {
		return sections.get(sectionName);
	}
	
	public  Section getSectionOfProduct(String productName) {
		return products.get(productName);
	}
	
	public  String getActivityFromSpeech(String speech) {
		
		//if we had had the time we would have filtered out words like "me" and "to" and quit words, in order to analyse only action+object
		speech = speech.trim().toLowerCase();
		String[] words = speech.split(" ");
		String action;
		String actionWords="";
		for (int i=0; i< words.length-2; i++) //first words, until the object word
			actionWords+=(words[i])+" ";
		actionWords=actionWords.trim();
		action=this.getCategory(actionWords);
		String object = this.getCategoryOfItem(words[words.length-2]); //the last one is the "quit" word
		System.out.println("action: "+action+" object: "+object);
		Activity candidate = new Activity(action,object,null);
		for (Activity act: activities) {
			if (act.equals(candidate))
				return act.getName();
		}
		 return null;
		 
		
	}
	
	public String getCategoryOfItem(String item) {
		if (sections.containsKey(item))
			return "section";
		if (products.containsKey(item))
			return "product";
		return null;
	}
	
	public String getActivityFromText(String text) {
		text = text.trim().toLowerCase();
		String[] words = text.split(" ");    
		for (int i = 0; i < words.length; i++) {
			String word = words[i];
			if (sections.containsKey(word))
				return "SelectSection";
			if (products.containsKey(word))
				return "SearchProduct";
		}
		return null;

	}
	
	public String recognizeSpeech() {
		return recognizer.recognizeSpeech();
	}
	
	public void deactivateRecognizer() {
		recognizer.deactivate();
	}
	
}
