package application.model;

import java.util.ArrayList;
import java.util.HashMap;

public class Grammar {
	
	private HashMap<String,String> categoriesByWord;
	
	public Grammar(){
		categoriesByWord = new HashMap<String,String>();
	}

	public void addWord(String word, String category) {
		categoriesByWord.put(word,category);
	}
	
	public String getCategory(String word) {
		return categoriesByWord.get(word);
	}
	
	public ArrayList<String> getWordsByCategory(String category){
		ArrayList<String> result = new ArrayList<String>();
		for (String word : categoriesByWord.keySet()) {
			if (categoriesByWord.get(word).equals(category))
				result.add(word);
		}
		return result;
	}
}
