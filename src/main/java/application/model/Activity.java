package application.model;

public class Activity {
	
	private String action;
	private String object;
	private String name;
	
	public Activity(String action, String object, String name) {
		this.action = action;
		this.object = object;
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean equals(Object other) {
		Activity act = (Activity)other;
		return this.action.equals(act.getAction()) && this.object.equals(act.getObject());
	}

	private Object getAction() {
		return action;
	}

	private Object getObject() {
		return object;
	}

}
