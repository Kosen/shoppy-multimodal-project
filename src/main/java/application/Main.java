package application;


import java.io.IOException;

import application.reasoner.Reasoner;
import application.view.SceneNavigator;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Shoppy");
                
        initRootLayout();

        showScene();
        	
		Reasoner.initialise();
		
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
		        public void handle(WindowEvent we) {
		            Reasoner.saveModel(); //just for debugging, not in final version
		        }
		    });    
    }
    
       


    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
        	
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/RootLayout.fxml")); 
            //Do not use SceneNavigator.* for this one because RootLayout is the parent node
            rootLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void showScene() {
    	SceneNavigator.setRootLayout(rootLayout);
    	SceneNavigator.loadScene(SceneNavigator.settings);
    }


    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}