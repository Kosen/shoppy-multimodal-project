package application.view;


import application.reasoner.Reasoner;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;

public class LayoutController_Settings {
	
	@FXML
	private RadioButton blindRB;
	@FXML
	private RadioButton deafRB;
	@FXML
	private RadioButton regularRB;
	@FXML
	private CheckBox handsCB;
	
	@FXML
	private ToggleGroup handicap;
	
	@FXML
	private CheckBox locationCBSect;
	
	@FXML
	private Button initialLocationB;
	
	@FXML
	private Button okB;
	
	
	@FXML
	public void moveToStart(ActionEvent event) {
		SceneNavigator.loadScene(SceneNavigator.start);
	}
	
	
	@FXML 
	private void updateHandicap() {
		Reasoner.updateBlind(blindRB.isSelected());
		Reasoner.updateDeaf(deafRB.isSelected());
		Reasoner.execQuery(Reasoner.IM_QUERY);
		
		
	}

	
	@FXML
	public void updateHandsStatus(ActionEvent event)  {
		Reasoner.updateHandsStatus(handsCB.isSelected());
		Reasoner.execQuery(Reasoner.IM_QUERY);


	}
	
	@FXML
	public void updateLocation(ActionEvent event){
		Reasoner.updateLocation( ((CheckBox) event.getTarget()).isSelected());
		Reasoner.execQuery(Reasoner.IM_QUERY);

	}
	
	@FXML
	public void updateInitialLocation(ActionEvent event)  {
		Reasoner.updateInitialLocation();
		initialLocationB.setDisable(true);
		locationCBSect.setDisable(false);
		Reasoner.execQuery(Reasoner.IM_QUERY);
		okB.setDisable(false);


	}
	
}
