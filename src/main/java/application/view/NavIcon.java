package application.view;

import application.navigation.Landmark;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;

public class NavIcon { 
	public String url;
	public double offset_x;
	public double offset_y;
	public double scale_ratio;
	public Image image;
	public Point2D position;
	
	public NavIcon(String URL, double offset_x, double offset_y, double scale_ratio) {
	      this.url = URL;
	      this.scale_ratio = scale_ratio;
	      //this.image = new Image (mapPictureLocation,width_canvas,height_canvas,true,true);
	      this.image = new Image(URL, 50*scale_ratio, 50*scale_ratio, true, true);
	      
	      this.offset_x = offset_x*scale_ratio;
	      this.offset_y = offset_y*scale_ratio;
	   }
	
	public void drawIcon(Canvas myCanvas) {
			myCanvas.getGraphicsContext2D().drawImage(image, position.getX()-offset_x, position.getY()-offset_y);
		}

	public void setPosition(Point2D position) {
			this.position = position;
		}
	public void setPosition(Landmark landmark) {
			this.position = new Point2D(landmark.x, landmark.y);
		}
	}