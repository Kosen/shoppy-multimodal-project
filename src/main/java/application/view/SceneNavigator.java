package application.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

import application.Main;

public class SceneNavigator {
	
	/*
	 SceneNavigator help to switch between several scene inside the rootLayout container
	 */
	
	/**
	 * Convenience constants for fxml layouts managed by the navigator.
	 */
	public static final String start = "view/StartScreen.fxml";
	public static final String settings = "view/SettingsScreen.fxml";

	/** The main application layout controller. */
	private static BorderPane rootLayout;
	
	public static void setRootLayout(BorderPane rootLayout) {
		SceneNavigator.rootLayout = rootLayout;
	}


	public static void loadScene(String fxml) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource(fxml));
            AnchorPane currentScreen = (AnchorPane) loader.load();
            rootLayout.setCenter(currentScreen);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}