package application.view;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class MapSupermarket {
	private static String mapPictureLocation = "/application/view/resources/map.png";

	
	public static String getMapPictureLocation() {
		return mapPictureLocation;
	}
	
	public class mapBlock 
	{
		public Color color = Color.BLUE;
		double pos_x;
		double pos_y;
		double width;
		double height;
		
		public List<double[]> sectionBorder;
		public double id;
		
		//list of mapSection = graphical representation of section
		
		public mapBlock(double pos_x, double pos_y, double width, double height) 
		{
			super();
			this.pos_x = pos_x;
			this.pos_y = pos_y;
			this.width = width;
			this.height = height;
			this.sectionBorder = SetSectionBorder();
		}

		private List<double[]> SetSectionBorder() {
			
			double width_section = width/3;
			double height_section = height/3;
			
			double pos_x_inner = pos_x+width_section;
			double pos_y_inner = pos_y+width_section;
			double width_inner = width - 2*width_section;
			double height_inner = height -2*height_section;
			
			sectionBorder = new ArrayList<double[]>();
			sectionBorder.add(new double[]{pos_x,pos_y,pos_x_inner,pos_y_inner}); //left-up
			sectionBorder.add(new double[]{pos_x,pos_y+height,pos_x_inner,pos_y_inner+width_inner}); //left-down
			
			sectionBorder.add(new double[]{pos_x+width,pos_y,pos_x_inner+width_inner,pos_y_inner}); //right-up
			sectionBorder.add(new double[]{pos_x+width,pos_y+height,pos_x_inner+width_inner,pos_y_inner+height_inner}); //right-down
			
			sectionBorder.add(new double[]{pos_x_inner,pos_y_inner,width_inner,height_inner}); //inner rectangle shape
			
			return sectionBorder;
		}
		
		public GraphicsContext Draw() { // Not working right now 
			Canvas canvas = new Canvas(250,250);
			GraphicsContext gc = canvas.getGraphicsContext2D(); 
			gc.fillRect(pos_x, pos_y, width, height);
			for (int i = 0; i < sectionBorder.size();i++)
			{
				if (i < sectionBorder.size() -1)
				{
					//Draw the separative line
					gc.strokeLine(sectionBorder.get(i)[0],sectionBorder.get(i)[1],sectionBorder.get(i)[2],sectionBorder.get(i)[3]);
				}
				else 
				{
					//Draw the inner rectangle
					gc.strokeRect(sectionBorder.get(i)[0],sectionBorder.get(i)[1],sectionBorder.get(i)[2],sectionBorder.get(i)[3]);
				}
			}
			return gc;
		}
		
	}

	public class mapSection 
	{
		public Color colorSelected = Color.GREEN;
		public double[] shape;
		public String sectionName;
		
		public mapSection(double[] shape) 
		{
			this.shape = shape;
		}
	}
	
	public Color colorBackground = Color.RED;

	
	public void DrawMap() { // Not working right now
		
		double height_block = 100;
		double width_block = 220;
		
		//myRefCanvas.getGraphicsContext2D().setFill(colorBackground);
		
		
		mapBlock block1 = new mapBlock(60, 60, width_block, height_block);
		mapBlock block2 = new mapBlock(240, 60, width_block, height_block);
		mapBlock block3 = new mapBlock(90, 370, height_block, width_block);
		
		block1.Draw();
		block2.Draw();
		block3.Draw();
		
		return;
	}
}
