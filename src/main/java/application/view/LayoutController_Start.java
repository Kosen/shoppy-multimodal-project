package application.view;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import application.Main;
import application.reasoner.Reasoner;

public class LayoutController_Start {
	
	
	private static final String canvasDisplay = "view/CanvasDisplay.fxml";
	
	@FXML
	private Pane canvas_pane;
	
	
	@FXML
    private void initialize() {
		//System.out.println("start screen loaded");
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource(canvasDisplay));
            AnchorPane canvasPane = (AnchorPane) loader.load();
            canvas_pane.getChildren().setAll(canvasPane); 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
    }
	
	
	@FXML
	public void moveToSettings(ActionEvent event) {
		SceneNavigator.loadScene(SceneNavigator.settings);
	}
	
}
