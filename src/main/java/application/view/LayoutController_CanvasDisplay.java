package application.view;

import java.util.ArrayList;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.util.Duration;
import application.navigation.Landmark;
import application.navigation.NavigationController;
import application.reasoner.Reasoner;

public class LayoutController_CanvasDisplay  {
	
	private static String vImpairedPictureLocation = "/application/view/resources/visually_impaired.png";
	//private static mapNavigationInfo ; // Need to be implemented
	
	private static Image map_Image, vIpaired_Image;
	private double width_canvas;
	private double height_canvas;
	
	private NavIcon navIcon_here = new NavIcon("/application/view/resources/here-icon.png", 25, 50 ,1);
	private NavIcon navIcon_destination = new NavIcon("/application/view/resources/destination-icon.png", 8 , 48 , 1 );
	
	private NavigationController navigationController = new NavigationController();
	private Landmark[] path = navigationController.getPath();
	private int nextStepB_status = 0;
	
	
	@FXML
	private Button speakB;
	
	@FXML
	private Canvas layer_mapPicture,layer_navPath,layer_visuallyImpaired;
	
	@FXML
	private HBox hBoxTextSearch;
	
	@FXML
	private TextField searchTF;
	
	@FXML
	private Button searchB;
	
	@FXML
	private Button nextStepB;



			
	@FXML
    private void initialize() {
		
		width_canvas = layer_mapPicture.getWidth();
		height_canvas = layer_mapPicture.getHeight();
		map_Image = new Image(MapSupermarket.getMapPictureLocation(),width_canvas,height_canvas, true,true);
		layer_mapPicture.getGraphicsContext2D().drawImage(map_Image, 0, 0);
		vIpaired_Image =  new Image(vImpairedPictureLocation,100,100, true,true);
		layer_visuallyImpaired.getGraphicsContext2D().drawImage(vIpaired_Image, width_canvas/2-50,height_canvas/2-50);
		hideAllLayers();
		showIM();
		
			
	}
	
	private void showIM() {
		boolean activateSpeech=false;

		ArrayList<String> imList = Reasoner.getNextIM();
		for (String im : imList) {
			switch (im) { 
				case "GraphicsModality": showMap(true); break;
				case "TextModality":	 showTextSearch(true); break;
				case "TouchModality":	 showMap(true); break;
				case "SpeechInputModality": showNonVisualModality(true);
											activateSpeech=true;
											break;
				case "ScanModality":   //show camera feed: not implemented in this demo 
										//showNonVisualModality(true);
										break;
			}
			
			
		}
		
		if (activateSpeech) {
			Timeline timeline = new Timeline(new KeyFrame(//this is a hack to overcome the recogniser activating before canvas rendering
			        Duration.millis(500),
			        ae -> activateRecognizer(null)));
			timeline.play();
		}
	}
	
	
	
	private void hideAllLayers() {
		showNonVisualModality(false);
		hBoxTextSearch.setVisible(false);
		layer_mapPicture.setVisible(false);
		layer_navPath.setVisible(false);  //not sure if this would be shown only when activity is "navigating"!
		nextStepB.setVisible(false);

	}
	private void showMap(boolean value) {
		layer_mapPicture.setVisible(value);
		
		//for navigation:
		//layer_navPath.setVisible(value);  //not sure if this would be shown only when activity is "navigating"!
		//nextStepB.setVisible(value);
		//if (value)
		//	updateNextStepB(null);

		showNonVisualModality(false);

	}
	
	private void showTextSearch(boolean value) {
		hBoxTextSearch.setVisible(value);
		showNonVisualModality(false);

	}
	
	private void showNonVisualModality(boolean value) {
		layer_visuallyImpaired.setVisible(value);
		layer_visuallyImpaired.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			   public void handle(MouseEvent event) {
					System.out.println("entering listener");
					Reasoner.deactivateRecognizer();
				} 

				});

		

	}
	
	


	public void setPath(Landmark[] path )
	{
		this.path = path;
	}
	


	public void displayRoadtoItem() {
		
		layer_navPath.getGraphicsContext2D().clearRect(0, 0, width_canvas, height_canvas);  //clean the canvas
		
		int length_path = path.length;
		int currentStep = navigationController.getCurrentStep();
		
		navIcon_here.setPosition(path[currentStep]);
		navIcon_destination.setPosition(path[length_path-1]);
		
		for( int i = 0; i < length_path -1; i++)
		{
			if(i >= currentStep)
			{
				drawLine(layer_navPath, path[i], path[i+1]);
			}
		}
		
		navIcon_here.drawIcon(layer_navPath);
		navIcon_destination.drawIcon(layer_navPath);
		
		if(currentStep == length_path -2)
		{
			nextStepB_status = 2;
		}
		
		voce.SpeechInterface.synthesize(path[currentStep].getVoiceOutput()); // voce navigation info
	}
	
	private void drawLine(Canvas myCanvas, Landmark lmark1, Landmark lmark2) {
		myCanvas.getGraphicsContext2D().strokeLine(lmark1.x, lmark1.y, lmark2.x, lmark2.y);
	}
	
	public void moveToNextStep() {
		displayRoadtoItem();
		this.navigationController.nexStep();
		
	}
	
	@FXML
	public void updateNextStepB(ActionEvent event) {

		if ( nextStepB_status == 0)
		{
			nextStepB_status = 1;
		}
		else if( nextStepB_status == 1)
		{
			nextStepB.setText("Move to next step");
			this.moveToNextStep();
			
		}
		else if (nextStepB_status == 2)
		{
			nextStepB.setText("Reset");
			this.moveToNextStep();
			nextStepB_status = 3;
		}
		else if (nextStepB_status == 3)
		{
			this.reset();
		}
	}

	private void reset() {
		layer_navPath.getGraphicsContext2D().clearRect(0, 0, width_canvas, height_canvas); //clean all of the canvas
		navigationController.resetCurrentStep();  //reset the navigation currentStep
		nextStepB_status = 0;
		updateNextStepB(null);
	}
	
	@FXML
	public void searchByText(ActionEvent event)
	{
			Reasoner.updateInputModality("TextModality");
			Reasoner.updateActivityFromText(searchTF.getText());
			Reasoner.execQuery(Reasoner.OM_QUERY);
			ArrayList<String> om = Reasoner.getNextOM();
			//TODO: decide which om to take when there are several in the list
			//TODO: implement actual search, and retrieve the location of the product in the correct OM
			Reasoner.execQuery(Reasoner.IM_QUERY);
			//TODO: show corresponding IMs

	}
	
	@FXML
	public void activateRecognizer(ActionEvent event) {
		
		speakB.setStyle("-fx-border-color: rgba(255,0,0,0.5) ;-fx-border-width:3px ; -fx-border-radius: 3px;");

		Reasoner.updateInputModality("SpeechInputModality");
		
		
		String speech = Reasoner.recognizeSpeech();
		setSpokenText(speech);
		//speakB.setStyle(null);
		Reasoner.updateActivityFromSpeech(speech);

		Reasoner.execQuery(Reasoner.OM_QUERY);
		ArrayList<String> om = Reasoner.getNextOM();
		//TODO: implement actual activity, and retrieve the result in the correct OM

		Reasoner.execQuery(Reasoner.IM_QUERY);		
		showIM();
	}
	
	public void setSpokenText(String speech) {
		searchTF.setText(speech);
	}

	
	
	
}
