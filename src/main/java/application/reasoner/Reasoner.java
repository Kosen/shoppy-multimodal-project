package application.reasoner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;

import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFHandler;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.helpers.StatementCollector;
import org.openrdf.rio.rdfxml.RDFXMLParser;
import org.openrdf.rio.rdfxml.RDFXMLWriter;
import org.openrdf.sail.memory.MemoryStore;

import application.model.Supermarket;

public class Reasoner {
	
	private static Repository repo;
	private static RepositoryConnection con;
	private static String PREFIXIMI= "http://intelligent-multimodal-interaction.org/relations/";
	public static String PREFIXBASE= "http://intelligent-multimodal-interaction.org/concepts/";
	private static final int BOOL_LITERAL=0;
	private static final int RES_LOCATION=1;
	private static final int RESOURCE=2;
	private static final String QUERY_PREFIX = "PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n"
			+ "PREFIX imi:   <http://intelligent-multimodal-interaction.org/relations/> \n"
			+ "PREFIX base:  <http://intelligent-multimodal-interaction.org/concepts/> \n";
	public static final int IM_QUERY=0;
	public static final int OM_QUERY=1;
	
	private static Supermarket supermarket;
	
	private static ArrayList<String> nextIM;
	private static ArrayList<String> nextOM;


	public static void initialise()  {
		repo = new SailRepository(new MemoryStore());
		try {
			repo.initialize();
			con = repo.getConnection();
			URL doc = (new File("model.rdf")).toURI().toURL();
			InputStream inputStream = doc.openStream();
			RDFParser rdfParser = new RDFXMLParser(); 
			ArrayList<Statement> myList = new ArrayList<Statement>();
			StatementCollector collector = new StatementCollector(myList);
			rdfParser.setRDFHandler(collector);
			
			rdfParser.parse(inputStream, doc.toString());
			for (Statement statement: collector.getStatements())
				con.add(statement);

		} catch (RepositoryException | IOException | RDFParseException | RDFHandlerException e) {
			e.printStackTrace();
		}
		supermarket = new Supermarket();
		
		
	}

	
	private static Literal getLiteralValue(ValueFactory f, Boolean newValue ) {
		String literal;
		if (newValue.booleanValue()) //if handicap user
			literal="true";  //then in the old statement he's not handicapped and "has..."
		else
			literal="false";
		return f.createLiteral(literal);
	}
	
	
	private static URI getLocationResource(ValueFactory f, Boolean newValue ) {
		String location;
		if (newValue.booleanValue()) //the user is now in a section
			return null; //the user is still in the supermarket, we won't delete its statement!
		else
			location="Section"; //the user is no longer in a section, so this statement will be deleted
		return f.createURI(PREFIXBASE+location);
				
	}
	
	private static URI getNewActivityResource(ValueFactory f, String newValue ) {
		
		return f.createURI(PREFIXBASE+newValue);
				
	}
	
	private static Value getOldResource(ValueFactory f, RepositoryConnection con, String objectStr ) {
		
		String prefix = "PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n"
				+ "PREFIX imi:   <http://intelligent-multimodal-interaction.org/relations/> \n"
				+ "PREFIX base:  <http://intelligent-multimodal-interaction.org/concepts/> \n";

		
		String queryString = prefix+"SELECT ?"+objectStr +" WHERE {" +
									"?currentNode imi:"+objectStr +" ?"+objectStr +" . " + 
									"}";  
		
		TupleQuery tupleQuery;
		try {
			tupleQuery = con.prepareTupleQuery(QueryLanguage.SPARQL, queryString);
			TupleQueryResult result = tupleQuery.evaluate();
			
			Value valueOfCA =null ;
			if (result.hasNext()) {
				BindingSet bindingSet = result.next();
				valueOfCA= bindingSet.getValue(objectStr);
			}
			return valueOfCA;

		} catch (RepositoryException|MalformedQueryException|QueryEvaluationException e) {
			e.printStackTrace();
		} 
		return null;
		
				
	}
	
	
	public static void updateActivityFromText(String text) {
				
		String activity = supermarket.getActivityFromText(text);
		updateActivity(activity);
		

	}
	
	public static void updateActivityFromSpeech(String speech) {
		
		String activity = supermarket.getActivityFromSpeech(speech);
		System.out.println("activity detected: "+activity);
		updateActivity(activity);
		

	}
	
	public static String recognizeSpeech() {
		return supermarket.recognizeSpeech();
	}
	
	public static void deactivateRecognizer() {
		supermarket.deactivateRecognizer();
	}
	
	public static void updateActivity(String newActivity) {
		if (newActivity!=null) { //if the search has no results, we don't update the model or run the reasoner
			updateModel("CurrentActivity", "currentActivity", RESOURCE, newActivity);
		}
	}
	
	public static void updateInputModality(String newIM) {
		
		updateModel("CurrentInputModality", "currentInputModality", RESOURCE, newIM);

	}
	
	public static void saveModel(){

	try {
	      
	      File outputFile= new File("modelDebug.rdf"); //just for debugging; in the final version we won't even save the results because it's useless
	      OutputStream out;
		  try {
					out = new FileOutputStream(outputFile);
					RDFHandler rdfxmlWriter = new RDFXMLWriter(out);
				    con.export(rdfxmlWriter);
		   } 
		  catch (FileNotFoundException e) {
					e.printStackTrace();
		   } 
		  catch (RDFHandlerException e) {
					e.printStackTrace();
		   } catch (RepositoryException e) {
			   		e.printStackTrace();
		}			      
	   }
	   finally {
	      try {
			con.close();
	       } catch (RepositoryException e) {
	    	  e.printStackTrace();
		}
	   }
	   
	}
	
	
	public static void updateModel(String subjectStr, String objectStr, int statementType, Object newValue) {
		ValueFactory f = repo.getValueFactory();
		//Prepare statements
		URI subject = f.createURI(PREFIXBASE+subjectStr);
		URI object = f.createURI(PREFIXIMI+objectStr);
		Value oldValue =null;
		Value finalValue =null;
		try {
			   RepositoryConnection con = repo.getConnection();	
				switch (statementType) {  //this should have been done with classes and polymorphism but no time :(
					case BOOL_LITERAL: oldValue = getLiteralValue(f,(Boolean)newValue);
								  	   finalValue = getLiteralValue(f,new Boolean(! ((Boolean)newValue).booleanValue())); //horrible
								  	   break;
					case RES_LOCATION: oldValue = getLocationResource(f,(Boolean)newValue);
					  			  	   finalValue = getLocationResource(f,new Boolean(! ((Boolean)newValue).booleanValue())); //horrible
					  			  	   break;
					case RESOURCE:     oldValue = getOldResource(f,con,objectStr);
									   finalValue = getNewActivityResource(f,(String)newValue);
					  				   break;
				}	
				   
			   //Remove old statement
				if (oldValue!=null)
					con.remove(f.createStatement(subject, object, oldValue));	
				//Add new statement
				if (finalValue!=null)
					con.add(subject, object, (Value)finalValue); 

				//saveModel();
			  
		}
		catch (Exception e) {
		}
	}
	
	public static void updateBlind(boolean newValue) {
		updateModel("CurrentUserSight", "currentUserSight", BOOL_LITERAL, newValue);
		// updateActivity("SearchProduct"); //this is for debugging, until we have the updateActivity called where it belongs
		// updateInputModality("TouchModality"); //this is for debugging, until we have the updateActivity called where it belongs

		
	}
	
	public static void updateDeaf(boolean newValue) {
		
		updateModel("CurrentUserHearing", "currentUserHearing", BOOL_LITERAL, newValue);
		
	}
	
	public static void updateHandsStatus(boolean newValue){
		
		updateModel("HandStatus", "currentHandsStatus", BOOL_LITERAL, newValue);
		
	}
	
	public static void updateLocation(boolean newValue) {
		
		updateModel("CurrentLocation", "currentLocation", RES_LOCATION, newValue);
		
		
	}
	
public static void updateInitialLocation() {
		
		updateModel("CurrentLocation", "currentLocation", RESOURCE, "Supermarket");
		
		
	}
	
	private static String getIMQuery() {
		String queryString = QUERY_PREFIX+"SELECT DISTINCT  ?nextIM WHERE {" +
				" ?nextIM rdf:type base:InputModality . " +
					
				"?userTypeNode imi:has_sight ?sees ." +
				"?userTypeNode imi:has_hearing ?hears ." +
				
				"?currentUserSightNode imi:currentUserSight ?sees . " +
				
				"?currentUserHearingNode imi:currentUserHearing ?hears . " +
				
				"?currentHandsStatusNode imi:currentHandsStatus ?handsFree . " +
					
				"{ ?userTypeNode imi:can_use 'any' . } " +
				" UNION { ?userTypeNode imi:can_use  ?blankNodeNextI . " + 
				" { ?blankNodeNextI rdf:type ?parent .  ?nextIM rdf:type ?parent .	}"	+		
				" UNION { ?blankNodeNextI rdf:type ?nextIM .} }" +
				
				"  { { ?nextIM imi:require-hands-free 'false' . } " +
				"UNION { ?nextIM imi:require-hands-free  ?handsFree .} }" +
				" UNION { ?userTypeNode rdf:type base:Handicap . }  " + 
				
				" {  { ?nextIM rdf:type base:NonInvasiveModality . }" +
				"    UNION "
				+ "  { ?nextIM rdf:type  ?parent . " + 
				"      ?parent rdf:type base:NonInvasiveModality  .  } }"+
				
				" UNION { { ?nextIM rdf:type base:InvasiveModality .  "+
				"  ?currentHandsStatusNode imi:currentHandsStatus 'false' . }"+
				"UNION { ?nextIM rdf:type base:InvasiveModality ." +
				"?currentUserSightNode imi:currentUserSight 'false' . } }"+  
				//we'll only activate an invasive IM if the user is blind or has his hands busy
			
			"}";
		return queryString;
	}

	private static String getOMQuery() {
		String queryString = QUERY_PREFIX+"SELECT DISTINCT ?currentActivity ?currentLocation ?currentIM ?sees ?hears ?handsFree ?om ?nextIM WHERE {" +
				 
				"?currentActivityNode imi:currentActivity ?currentActivity . " +
				"?currentLocationNode imi:currentLocation ?currentLocation . " +
				"?currentActivity imi:required-location ?currentLocation . " +
				
				"?currentIMNode imi:currentInputModality ?currentIM . " +
				"?currentIM rdf:type base:InputModality . " +
												
				"?userTypeNode imi:has_sight ?sees ." +
				"?userTypeNode imi:has_hearing ?hears ." +
				
				"?currentUserSightNode imi:currentUserSight ?sees . " +
				
				"?currentUserHearingNode imi:currentUserHearing ?hears . " +
				
				"?currentHandsStatusNode imi:currentHandsStatus ?handsFree . " +
											
				"{ ?userTypeNode imi:can_use 'any' . } " +
								
				" UNION { ?userTypeNode imi:can_use  ?blankNodeI . " + 
				" { ?blankNodeI rdf:type ?parent .  ?currentIM rdf:type ?parent .	}"	+		
				" UNION { ?blankNodeI rdf:type ?currentIM .} }" +
				
				" ?om rdf:type base:OutputModality . " +
				
				" { { { ?om imi:require-hands-free 'false' . } " +
				"UNION { ?om imi:require-hands-free  ?handsFree .} }" +
				" UNION { ?userTypeNode rdf:type base:Handicap . } } " + 	// this puts handicap over hands status.
				" UNION {  ?currentIM rdf:type base:PrivateModality .  }"+ 	// this puts privacy over hands status. 
				
				"{ ?userTypeNode imi:can_use 'any' . } " +

				" UNION { ?userTypeNode imi:can_use  ?blankNodeO . " + 
				
				" { ?blankNodeO rdf:type ?parent . ?om rdf:type ?parent .	}"	+
				" UNION {?blankNodeO rdf:type ?om . } }" +
				
				" { ?currentIM rdf:type base:NonVisualModality . }" +
				
				" UNION { ?currentIM rdf:type base:NonAudioModality . ?om rdf:type base:NonAudioModality . }" +
				
				
				" ?currentActivity imi:has-constraint-input ?blankNodeCAI . " +
				" { ?blankNodeCAI rdf:type ?parentI .  ?currentIM rdf:type ?parentI . } " +
				" UNION { ?blankNodeCAI rdf:type ?currentIM . } " +
				
				
				" ?currentActivity imi:has-constraint-output ?blankNodeCAO . " +
				"{ ?blankNodeCAO rdf:type ?parentO .  ?om rdf:type ?parentO . }" +
				" UNION { ?blankNodeCAO rdf:type ?om . } " +
			     
				"}";  
			return queryString;
	}
	
	
	
	private static ArrayList<String> processOMQueryResult(TupleQueryResult result)  {
		ArrayList<String> omList = new ArrayList<String>();
		try {
			if (result.hasNext()) {
				BindingSet bindingSet = result.next();
				
				//this is just to print the result of the query, comment it if needed
				Value valueOfCA = bindingSet.getValue("currentActivity");
				Value valueOfCL = bindingSet.getValue("currentLocation");
				Value valueOfCIM = bindingSet.getValue("currentIM");
				Value valueOfSees = bindingSet.getValue("sees");
				Value valueOfHears = bindingSet.getValue("hears");
				Value valueOfHands = bindingSet.getValue("handsFree");
				Value valueOfOm = bindingSet.getValue("om");		
				System.out.println("activity: "+valueOfCA +"\nlocation: " +valueOfCL + "\nIM: " + valueOfCIM + "\nSees?: "+ valueOfSees +
						"\nHears?: "+ valueOfHears +
						 "\nHands free?: "+ valueOfHands +
						 "\nOutput modalities for this user: \n" + valueOfOm);
				
				while (result.hasNext()) {	
					bindingSet = result.next();
					valueOfOm = bindingSet.getValue("om");
					System.out.println(valueOfOm );
					String omFull = valueOfOm.toString();
					omList.add(omFull.substring(omFull.lastIndexOf("/")+1,omFull.length())); //adding OutputModality to the processed list

				}
			}
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		}
		return omList;
	}
	
	private static ArrayList<String> processIMQueryResult(TupleQueryResult result) {
		ArrayList<String> imList = new ArrayList<String>();
		System.out.println("\nNext input modalities for this user: \n"); 
		BindingSet bindingSet;
		try {
			while (result.hasNext()) {	
				bindingSet = result.next();
				Value valueOfNextIm = bindingSet.getValue("nextIM");
				System.out.println(valueOfNextIm );
				String imFull = valueOfNextIm.toString();
				imList.add(imFull.substring(imFull.lastIndexOf("/")+1,imFull.length())); //adding InputModality to the processed list
			}
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		}
		return imList;

	}
	
	public static ArrayList<String> getNextIM() {
		return nextIM;
	}
	
	public static ArrayList<String> getNextOM() {
		return nextOM;
	}
	

	
	public static void execQuery(int queryToPerform) {

		
		String queryString = null;
		
		switch (queryToPerform)  {
			case OM_QUERY: queryString = getOMQuery();
						   System.out.println("\n\nQUERY for OM:\n");
				      		break;
			case IM_QUERY: queryString = getIMQuery();
							System.out.println("\n\nQUERY for IM:\n");
					   		break;
		}
		
		TupleQuery tupleQuery;
		try {
			tupleQuery = con.prepareTupleQuery(QueryLanguage.SPARQL, queryString);
			TupleQueryResult result = tupleQuery.evaluate();

			switch (queryToPerform)  {

			case OM_QUERY: nextOM = processOMQueryResult(result);						
				      	   break;
			case IM_QUERY: nextIM = processIMQueryResult(result);
			}
			

			
		} catch (RepositoryException|MalformedQueryException | QueryEvaluationException e) {
			
			e.printStackTrace();
		}
		 
		
		
	}
	
}
