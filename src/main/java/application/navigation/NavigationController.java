package application.navigation;

public class NavigationController {
	
	private  Landmark[] path = {
			new Landmark(200,50,"move straight"),
			new Landmark(200,325, "turn left and move straight"),
			new Landmark(350,325, "turn right and move straight"),
			new Landmark(350,425, "you arrived to your destination")
	};

			
	private int currentStep;
	
	public NavigationController() {
		currentStep = 0 ;
	}

	public int getCurrentStep() {
		return currentStep;
	}

	/*public void setCurrentStep(int currentStep) {
		this.currentStep = currentStep;
	}*/

	public void nexStep() {
		//System.out.println("nextStep   : => currentStep:"+currentStep);
		this.currentStep ++;
		//System.out.println("             => currentStep:"+currentStep);
		
	}

	public void resetCurrentStep() {
		this.currentStep = 0;
		
	}
	
	public void driveMeTo(String item)
	{
		
	}

	public Landmark[] getPath() {
		return path;
	}
	
	
}
