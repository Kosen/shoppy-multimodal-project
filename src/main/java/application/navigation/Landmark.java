package application.navigation;

public class Landmark
{
	public double x;
	public double y;
	private String voiceOutput;
	
	public Landmark(double x, double y, String voiceOutput) {
		this.x = x;
		this.y = y;
		this.voiceOutput = voiceOutput;
	}

	public String getVoiceOutput() {
		return voiceOutput;
	}
}